import requests
from html.parser import HTMLParser
from urllib.parse import urlparse
import locale
import argparse

class MyWebCrawler(HTMLParser):

    def __init__(self, sUrl, iTargetLevel):
        HTMLParser.__init__(self)
        if iTargetLevel > 0:
            #imprime el sitio web actual
            print(sUrl) 
            self.word_count = {}
            self.links = []
            self.words = []
            self.body = False
            code = self.retrieveWebsite(sUrl)
            #HTML parsing
            self.feed(code)
            #parsing para los niveles mas bajos
            if iTargetLevel > 1:
                for link in self.links:
                    if "http" not in link:
                        #construccion del link completo para la referencia relativa
                        link = self.getBaseUrl(sUrl) + link 
                    w = MyWebCrawler(link, iTargetLevel - 1)
                    for word in w.words:
                         #agregacion de las palabras desde los niveles mas bajos
                        self.addWord(word)

    #eventhandler, activado con el argumento parseado en los tags de inicio
    def handle_starttag(self, tag, attrs): 
        if tag == "a": #encuentra link
            sLink = attrs[0][1]
            if "http" in sLink  or sLink[0] == "/":
                self.addLink(sLink)
        elif tag == "body": #solo usar las palabras del body
            self.body = True
    #eventhandler, activado con el argumento parseado en los tags de final
    def handle_endtag(self, tag): 
        if tag == "body":
            self.body = False
    #eventhandler, activado con el argumento parseado del dato
    def handle_data(self, data):
        if self.body:
            self.getWords(data) #capturando las palabras de body
    
    #base Url (sin path)
    def getBaseUrl(self, sUrl): 
        oUrl = urlparse(sUrl)
        return(oUrl.scheme + "://" + oUrl.netloc)
    
    #codigo completo del sitio web
    def retrieveWebsite(self, sUrl):
        sCode = requests.get(sUrl)
        return sCode.text
    
    #agregar palabra al catalogo (con control de repeticiones)
    def addWord(self, sWord):
         if sWord in self.words:
             self.word_count[sWord] +=1
         else:
             self.word_count[sWord] = 1
             self.words.append(sWord)
	
    #agrega los links al catalogo sin duplicados
    def addLink(self, sLink):
        if sLink not in self.links:
            self.links.append(sLink)        
    
    #busqueda de palabras en todo el texto
    def getWords(self, sText):
        sWord = ""
        sAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚüñÑ"
        sCapital = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÑ"

        for cLetter in sText:
            if (cLetter == " " or cLetter in sCapital):  #crear nueva palabra desde espacio o letra Capital
                if len(sWord) > 0: # add old word to catalog
                    self.addWord(sWord.upper())
                if cLetter == " ": #crear nueva palabra
                    sWord = ""
                else:
                    sWord = cLetter
            elif cLetter in sAlphabet: #agregar letra a la palabra
                sWord += cLetter
    
    #guardar el catalago de palabras con su respectiva cuenta al archivo
    def writeToFile(self, aArray, sFilename):
        oFile = open(sFilename, "w", encoding = locale.getpreferredencoding())
        sFileText = ""
        for sElement in aArray:
            sFileText+= str(self.word_count[sElement]) + ";" + sElement + "\n"
        oFile.write(sFileText)
        oFile.close()
    
# Muh Main
parser = argparse.ArgumentParser(description='Web crawler')
parser.add_argument('sitio', nargs='?', action='store', help='sitio raiz')
parser.add_argument('-l', action='store', default=2, help='maximo nivel de profundidad')

args = parser.parse_args()

crawlr = MyWebCrawler(args.sitio, int(args.l))
crawlr.writeToFile(crawlr.words, "test.txt")