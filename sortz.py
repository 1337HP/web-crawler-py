import argparse

def mergeSort(toSort, modo):
    if len(toSort) > 1:
        mid = len(toSort)//2 #find the middle index of the list
        left = toSort[:mid]
        right = toSort[mid:]

        #llama recursivamente mergesort en las 2 mitades
        mergeSort(left,modo)
        mergeSort(right,modo)

        #mezcla las 2 listas en una ordenada
        #vars para control de loop
        i = j = k = 0

        while i < len(left) and j < len(right):
            ileft = int(left[i].split(";")[modo])
            iright = int(right[j].split(";")[modo])
            if ileft > iright:
                toSort[k] = left[i]
                i+=1
            else:
                toSort[k] = right[j]
                j+=1
            k+=1

        #se agregan los elementos restantes de la lista
        while i < len(left):
            toSort[k] = left[i]
            i+=1
            k+=1
        while j < len(right):
            toSort[k] = right[j]
            j+=1
            k+=1

def mergeSortAl(toSort,mode):
    if len(toSort) > 1:
        mid = len(toSort)//2 #find the middle index of the list
        left = toSort[:mid]
        right = toSort[mid:]

        #llama recursivamente mergesort en las 2 mitades
        mergeSortAl(left,mode)
        mergeSortAl(right,mode)

        #mezcla las 2 listas en una ordenada
        #vars para control de loop
        i = j = k = 0

        while i < len(left) and j < len(right):
            ileft = left[i].split(";")[mode]
            iright = right[j].split(";")[mode]
            if ileft < iright:
                toSort[k] = left[i]
                i+=1
            else:
                toSort[k] = right[j]
                j+=1
            k+=1

        #se agregan los elementos restantes de la lista
        while i < len(left):
            toSort[k] = left[i]
            i+=1
            k+=1
        while j < len(right):
            toSort[k] = right[j]
            j+=1
            k+=1

def bubbleSort(some_list, mode):
    for x in range(0, len(some_list) -1):
        for y in range(0, len(some_list) -1 -x):
            ileft = int(some_list[y].split(";")[0])
            iright = int(some_list[y+1].split(";")[0])
            if ileft < iright:
                some_list[y], some_list[y+1] = some_list[y+1], some_list[y]
    return some_list

def bubbleSortAl(some_list, mode):
    for x in range(0, len(some_list) -1):
        for y in range(0, len(some_list) -1 -x):
            ileft = some_list[y].split(";")[1]
            iright = some_list[y+1].split(";")[1]
            if ileft > iright:
                some_list[y], some_list[y+1] = some_list[y+1], some_list[y]
    return some_list

# Muh Main
parser = argparse.ArgumentParser(description='Sorter')
parser.add_argument('-a',action='store', default=0, help='algoritmo de ordenamiento')
parser.add_argument('-m', action='store', default=0, help='modo de ordenamiento 0=CUENTA 1=PALABRA')
args = parser.parse_args()

toSort = []

#archivo de exportacion
exportFile = open("resultado.txt", "w")

with open("test.txt", "r") as importFile:
        toSort = importFile.readlines()

        if int(args.a) == 0 and int(args.m) == 0:
            mergeSort(toSort, int(args.m))
            print("MergeSort clave=CUENTA")
        if int(args.a) == 0 and int(args.m) == 1:
            mergeSortAl(toSort, int(args.m))
            print("MergeSort clave=PALABRA")
        if int(args.a) == 1 and int(args.m) == 0:
            bubbleSort(toSort, int(args.m))
            print("BubbleSort clave=CUENTA")
        if int(args.a) == 1 and int(args.m) == 1:
            bubbleSortAl(toSort, int(args.m))
            print("BubbleSort clave=PALABRA")

        #guarda la lista ordenada en 'resultado.txt'
        for num in toSort:
            exportFile.write(str(num))
        #exportFile.write("\n")

exportFile.close()