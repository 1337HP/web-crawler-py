import argparse

globvar = 0

def set_globvar(num):
    global globvar    # Needed to modify global copy of globvar
    globvar = num

def binarysearch(x,A,left,right):
    mid=left+round((right-left)/2)
    set_globvar(mid)
    if(x==A[mid]):
        return mid
    elif(right==left):
        return
    elif(x>A[mid]):
        left=mid+1
    else:
        right=mid
    return (binarysearch(x, A, left, right))

# Muh Main
parser = argparse.ArgumentParser(description='Buscador')
parser.add_argument('argumento',action='store', help='argumento de busqueda. Asegurese se haber corrido "python sortz.py -a 0 -m 1"')
args = parser.parse_args()

toSort = []
nada = {}

with open("merge.txt", "r") as importFile:
    toSort = importFile.readlines()

    for x in range (0, len(toSort)):
        nada[x] = toSort[x].split(";")[1].rstrip('\n')

ll=len(nada)-1

if (binarysearch(str(args.argumento),nada,0,ll)):
    print("palabra encontrada")
else:
    if(globvar == ll):
        print("NO hay. Palabras mas cercanas:", nada[globvar-1], nada[globvar])
    elif(globvar == 0):
        print("NO hay. Palabras mas cercanas:", nada[globvar], nada[globvar+1])
    elif(globvar != 0 and globvar != ll):
        print("NO hay. Palabras mas cercanas:", nada[globvar-1], nada[globvar], nada[globvar+1])