def main():
    infile = open("test.txt","r")
    wordlist = infile.readlines() #seperates each word by line
    first = infile.readline().strip()
    infile.close()
    letters = []
    for word in wordlist:
        for letter in word:
            letters.append(letter)
    letter_counts = {}
    for letter in letters:
        letter_counts[letter] = letter_counts.get(letter, 0) + 1

    alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","Á","É","Í","Ó","Ú","Ñ"]
    #alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    ocu = total = 0

    # Print out the results
    print("Elemento de Mayor CUENTA",wordlist[0])
    for alphabetletter in alphabet:
        total += int(letter_counts[alphabetletter])
    for alphabetletter in alphabet:
        ocu = int(letter_counts[alphabetletter])/total*100
        print(alphabetletter,":", letter_counts[alphabetletter] , "%.2f" % round(ocu,2), "%")

if __name__ == "__main__":
    main()